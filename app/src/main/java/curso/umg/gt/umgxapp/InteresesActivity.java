package curso.umg.gt.umgxapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class InteresesActivity extends AppCompatActivity {
    private ListView lv1;
    private List<String> listado;
    private ArrayAdapter<String> adapter;
    private String valor1;
    private String valor2;
    private String valor3;
    private RadioGroup radioGroup1;
    private RadioGroup radioGroup2;
    private RadioGroup radioGroup3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intereses);

        listado = new ArrayList<>();
        lv1= (ListView) findViewById(R.id.lv1 );
        radioGroup1 = (RadioGroup) findViewById(R.id.opciones1);
        radioGroup2 = (RadioGroup) findViewById(R.id.opciones2);
        radioGroup3 = (RadioGroup) findViewById(R.id.opciones3);
        adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,listado);
        lv1.setAdapter(adapter);
    }

    public void add(View view){

        if (radioGroup1.getCheckedRadioButtonId() == R.id.radio_si1) {
            valor1 = "- Android -";
        } else if (radioGroup1.getCheckedRadioButtonId() == R.id.radio_no1) {
            valor1 = "";
        }
        if (radioGroup2.getCheckedRadioButtonId() == R.id.radio_si2) {
            valor2 = "- Java -";
        } else if (radioGroup2.getCheckedRadioButtonId() == R.id.radio_no2) {
            valor2 = "";
        }
        if (radioGroup3.getCheckedRadioButtonId() == R.id.radio_si3) {
            valor3 = "- Spring -";
        } else if (radioGroup3.getCheckedRadioButtonId() == R.id.radio_no3) {
            valor3 = "";
        }
        if (radioGroup1.getCheckedRadioButtonId() == R.id.radio_no1 && radioGroup2.getCheckedRadioButtonId() == R.id.radio_no2 &&radioGroup3.getCheckedRadioButtonId() == R.id.radio_no3) {
            Toast.makeText(this, "Usted ha seleccionado a todos no", Toast.LENGTH_SHORT).show();
            listado.add("Ninguna");
        } else {
            Toast.makeText(this, "Ha seleccionado: "+ valor1 + valor2 + valor3, Toast.LENGTH_SHORT).show();
            listado.add(valor1 + valor2 + valor3);
        }
        adapter.notifyDataSetChanged();
        radioGroup1.clearCheck();
        radioGroup2.clearCheck();
        radioGroup3.clearCheck();
    }
}
