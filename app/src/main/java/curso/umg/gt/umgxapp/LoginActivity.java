package curso.umg.gt.umgxapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    private EditText et1, et2, et3, et4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        

        et1 = (EditText) findViewById(R.id.et1);
        et2 = (EditText) findViewById(R.id.et2);
        et3 = (EditText) findViewById(R.id.et3);
        et4 = (EditText) findViewById(R.id.et4);
    }

    public void aceptar(View view) {

        String nombres = et1.getText().toString();
        String apellidos = et2.getText().toString();
        String pass = et3.getText().toString();
        String confirmpass = et4.getText().toString();

        if (nombres.equals("") || apellidos.equals("") || pass.equals("") || confirmpass.equals("")) {
            Toast.makeText(this, "Existen campos vacíos", Toast.LENGTH_SHORT).show();
        } else {
            if (pass.equals(confirmpass)){
                Intent i = new Intent(this, InteresesActivity.class);
                startActivity(i);
            } else {
                Toast.makeText(this, "Las Contraseñas no Coinciden ", Toast.LENGTH_SHORT).show();

            }
        }
    }
    public void cancelar(View view) {
        finish();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}


